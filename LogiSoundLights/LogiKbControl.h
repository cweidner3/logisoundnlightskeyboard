#pragma once

class RGBcolors;

class LogiKbControl
{
public:
	LogiKbControl();
	~LogiKbControl();

	bool SetKbRGB(RGBcolors);

public:
	void run(void);

public:
	int majorNumLed;	// Logitech LED SDK Major Number
	int minorNumLed;	// Logitech LED SDK Minor Number
	int buildNumLed;	// Logitech LED SDK Build Number

	bool keepRunning;	// Running Loop Flag
};

class RGBcolors
{
public:
	RGBcolors();
	RGBcolors(int, int, int);
	~RGBcolors();

	void HSLtoRGB(int, int, int);
	void HSVtoRGB(int, int, int);
	void SetRGB(int, int, int);
	int	GetR();
	int	GetG();
	int	GetB();
	void SetR(int);
	void SetG(int);
	void SetB(int);
	int	GetRp();
	int	GetGp();
	int	GetBp();

private:
	int red, green, blue;		/* Byte values of the RGB */
	int redp, greenp, bluep;	/* Percent values of the RGB */

};