#include "DftColorsThreads.h"
#include "AudioHandler.h"
#include "LogiKbControl.h"
#include "fft_serial.h"

#include <portaudio.h>
#include <iostream>
#include <stdio.h>
#include <array>

using namespace std;

extern bool globalKeepRunning;

/* DFT colors Global Data */
AudioBuffer audioChunk;
//const double dftneutralizer = audioChunk.numSamples * pow(2, audioChunk.sampleSize * 8);
const double dftneutralizer = pow(2, 38);
bool needsNewData = true;
HANDLE mutexHandle;

/******************************************************************************/
/** Audio Thread                                                             **/
/******************************************************************************/

unsigned int __stdcall PAThread(void *v)
{
	AudioHandler *paObject = (AudioHandler *)v;
	DWORD waitResult;

	/* Check if PA was initialized */
	if (!paObject->IsNoError()) {
		cout << "PA failed to initialize" << endl;
		paObject->PrintErrorMessage();
		globalKeepRunning = false;
		return 1;
	}

	/* Configure the input stream */
	if (!paObject->ConfigInput()) {
		// TODO Something Failed
		cout << "PA failed to configure the input" << endl;
		paObject->PrintErrorMessage();
		globalKeepRunning = false;
		return 2;
	}

	/* Open the input stream */
	if (!paObject->ConnectToInput()) {
		// TODO something Failed
		cout << "PA failed to connect to input stream" << endl;
		paObject->PrintErrorMessage();
		globalKeepRunning = false;
		return 3;
	}

	/* Enable the Input Stream before running */
	if (!paObject->EnableInput()) {
		cout << "PA failed to enable the input stream" << endl;
		paObject->PrintErrorMessage();
		globalKeepRunning = false;
		return 4;
	}

	/* Enter the running loop */
	while (paObject->Run() && globalKeepRunning) {
		/* Lock the Mutex */
		waitResult = WaitForSingleObject(mutexHandle, 100);
		if (waitResult == WAIT_OBJECT_0) {
			if (needsNewData) {
				/* Transfer the data into the outBuffer */
				audioChunk = paObject->GetDataBuffer();

				/* Set the flag */
				needsNewData = false;
			}

			/* Unlock the Mutex */
			ReleaseMutex(mutexHandle);
		}
	}

	/* Disable the Input Stream before Exiting */
	if (!paObject->DisableInput()) {
		cout << "PA failed to disable the input stream" << endl;
		paObject->PrintErrorMessage();
		return 4;
	}

	return 0;
}

/******************************************************************************/
/** DFT Colors Threads                                                       **/
/******************************************************************************/

double runningAvg(double a, double b);
void chooseFFTColorsRGB(RGBcolors *color, double *cplxBuf);
void chooseFFTColorsHSL(RGBcolors *color, double *magbuf);

unsigned int __stdcall DFTcolorThread(void *v)
{
	/* Control Objects */
	float myaverage = 0;
	DWORD waitResult;
	dftColorsThreadsParams_t *params = (dftColorsThreadsParams_t *)v;
	char coloroption = params->option;

	/* LED Control Objects */
	LogiKbControl *ledCtrlObj = params->kbptr;
	RGBcolors colorObj(255, 255, 255);
	unsigned int whiteValue = 0;

	/* FFT Specific Variables and Arrays */
	double *w, *x, *y;
	w = new double[audioChunk.numSamples];
	x = new double[audioChunk.numSamples * 2];
	y = new double[audioChunk.numSamples * 2];
	

	/*  Pre-calcluate w array */
	cffti(audioChunk.numSamples, w);
	
	while (globalKeepRunning) {
		/* Lock the Mutex */
		waitResult = WaitForSingleObject(mutexHandle, 100);
		if (waitResult == WAIT_OBJECT_0) {
			if (needsNewData == false) {
				/* Unlock the Mutex */
				ReleaseMutex(mutexHandle);

				/* Calculate things */
				int16_t *ptr = (int16_t *) audioChunk.data;
				for (int i = 0; i < audioChunk.numSamples; i += 1) {
					x[2 * i] = abs((double)*(ptr + i));
					x[(2 * i) + 1] = 0.0;
				}

				/* Calculate FFT */
				cfft2(audioChunk.numSamples, x, y, w, 1);
				/* Convert to Magnitudes */
				for (int i = 0; i < audioChunk.numSamples; i++) {
					x[i] = sqrt(pow(y[2 * i], 2) * pow(y[(2 * i) + 1], 2));
				}

				/* Select new colors for keyboard */
				switch (coloroption) {
				case 'd': 
					chooseFFTColorsRGB(&colorObj, x);
					ledCtrlObj->SetKbRGB(colorObj);
					break;
				case 'h':
					chooseFFTColorsHSL(&colorObj, x);
					ledCtrlObj->SetKbRGB(colorObj);
					break;
				default:
					chooseFFTColorsRGB(&colorObj, x);
					ledCtrlObj->SetKbRGB(colorObj);
					break;
				}
				

				/* Set the needsNewData flag */
				waitResult = WaitForSingleObject(mutexHandle, INFINITE);
				needsNewData = true;
				ReleaseMutex(mutexHandle);

				Sleep(40);
			}
			else {
				/* Unlock the Mutex */
				ReleaseMutex(mutexHandle);
			}
		}
	}

	cout << "TEST: Last whiteValue: " << whiteValue << endl;

	/* Free memory */
	delete[] w;
	delete[] x;
	delete[] y;
	return 0;
}

/******************************************************************************/

double runningAvg(double a, double b)
{
	double result;
	if (a == (double)0) {
		return b;
	}
	result = a + b;
	result /= 2;
	return result;
}


/******************************************************************************/

/*
 * Testing Program
 * http://www.physics-chemistry-interactive-flash-animation.com/electricity_electromagnetism_interactive/oscilloscope_description_tutorial_sounds_frequency.htm
 * 
 * NOTE: Maximum value of the DFT is the maximum value of input times number of samples
 * i.e. (1024 samples * 2^(16 bits)) for 16 bit audio = 2^26 = 67,108,864
 */
void chooseFFTColorsRGB(RGBcolors *color, double *magbuf)
{
	//
	int basslow = 2, basshigh = 4;
	int midlow = 6, midhigh = 10;
	int treblow = 12, trebhigh = 18;
	double bassval = 0, midval = 0, trebval = 0;

	for (int i = basslow; i <= basshigh; i++) {
		bassval = runningAvg(bassval, magbuf[i]);
	}
	for (int i = midlow; i <= midhigh; i++) {
		midval = runningAvg(midval, magbuf[i]);
	}
	for (int i = treblow; i <= trebhigh; i++) {
		trebval = runningAvg(trebval, magbuf[i]);
	}


	bassval /= dftneutralizer;
	midval /= dftneutralizer;
	trebval /= dftneutralizer;
	bassval *= 255;
	midval *= 255;
	trebval *= 255;

	if (bassval > 255)
		bassval = 255;
	if (midval > 255)
		midval = 255;
	if (trebval > 255)
		trebval = 255;

	color->SetRGB((int)bassval, (int)midval, (int)trebval);
}

/******************************************************************************/

void chooseFFTColorsHSL(RGBcolors *color, double *magbuf)
{
	//
	int basslow = 2, basshigh = 4;
	int midlow = 6, midhigh = 10;
	int treblow = 12, trebhigh = 18;
	double bassval = 0, midval = 0, trebval = 0;

	for (int i = basslow; i <= basshigh; i++) {
		bassval = runningAvg(bassval, magbuf[i]);
	}
	for (int i = midlow; i <= midhigh; i++) {
		midval = runningAvg(midval, magbuf[i]);
	}
	for (int i = treblow; i <= trebhigh; i++) {
		trebval = runningAvg(trebval, magbuf[i]);
	}


	bassval /= (dftneutralizer * 4);
	midval /= (dftneutralizer * 4);
	trebval /= (dftneutralizer * 4);
	bassval *= 255;
	midval *= 255;
	trebval *= 255;

	if (bassval > 255)
		bassval = 255;
	if (midval > 255)
		midval = 255;
	if (trebval > 255)
		trebval = 255;

	double mag = sqrt(pow(bassval, 2) + pow(midval, 2) + pow(trebval, 2));
	if (mag > 255)
		mag = 255;

	color->HSVtoRGB(255 - bassval , mag, mag);
	//color->SetRGB((int)bassval, (int)midval, (int)trebval);
}
