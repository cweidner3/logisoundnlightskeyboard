#include "AudioHandler.h"

#include <iostream>

using namespace std;

/* #define SAMPLE_RATE  (17932) // Test failure to open with this value. */
#define SAMPLE_RATE			(44100)
#define FRAMES_PER_BUFFER	(2048)
#define NUM_CHANNELS		(1)		/* Default: 2 */
#define NUM_SECONDS			(1)		/* Default: 15 */
/* #define DITHER_FLAG     (paDitherOff)  */
#define DITHER_FLAG			(0)

/* @todo Underflow and overflow is disabled until we fix priming of blocking write. */
#define CHECK_OVERFLOW		(0)
#define CHECK_UNDERFLOW		(0)

/* Select sample format. */
#if 0
#define PA_SAMPLE_TYPE  paFloat32
#define SAMPLE_SIZE		(4)
#define SAMPLE_SILENCE	(0.0f)
#define CLEAR(a) memset((a), 0, FRAMES_PER_BUFFER * NUM_CHANNELS * SAMPLE_SIZE)
#define PRINTF_S_FORMAT "%.8f"
#elif 1
#define PA_SAMPLE_TYPE  paInt16
#define SAMPLE_SIZE		(2)
#define SAMPLE_SILENCE	(0)
#define CLEAR(a) memset((a), 0, FRAMES_PER_BUFFER * NUM_CHANNELS * SAMPLE_SIZE)
#define PRINTF_S_FORMAT "%d"
#elif 1
#define PA_SAMPLE_TYPE  paInt24
#define SAMPLE_SIZE		(3)
#define SAMPLE_SILENCE	(0)
#define CLEAR(a) memset((a), 0, FRAMES_PER_BUFFER * NUM_CHANNELS * SAMPLE_SIZE)
#define PRINTF_S_FORMAT "%d"
#elif 0
#define PA_SAMPLE_TYPE  paInt8
#define SAMPLE_SIZE(1)
#define SAMPLE_SILENCE(0)
#define CLEAR(a) memset((a), 0, FRAMES_PER_BUFFER * NUM_CHANNELS * SAMPLE_SIZE)
#define PRINTF_S_FORMAT "%d"
#else
#define PA_SAMPLE_TYPE  paUInt8
#define SAMPLE_SIZE(1)
#define SAMPLE_SILENCE(128)
#define CLEAR(a) {	\
     int i; \
     for( i=0; i<FRAMES_PER_BUFFER*NUM_CHANNELS; i++ ) \
         ((unsigned char *)a)[i] = (SAMPLE_SILENCE); \
 }
#define PRINTF_S_FORMAT "%d"
#endif

/******************************************************************************/
/** Constructors                                                             **/
/******************************************************************************/


AudioHandler::AudioHandler()
	: verbose(false)
{
	// Initialize the flag as true
	isRunning = false;

	// Initalize the module and make sure nothing went wrong
	errorCode = Pa_Initialize();
	if (errorCode != paNoError) {
		// Something went wrong
		isRunning = false;
	}
}

AudioHandler::AudioHandler(bool verb)
	: verbose(verb)
{
	// Initialize the flag as true
	isRunning = true;

	// Initalize the module and make sure nothing went wrong
	errorCode = Pa_Initialize();
	if (errorCode != paNoError) {
		// Something went wrong
		isRunning = false;
		if(verbose)
			PrintErrorMessage();
	}
}



AudioHandler::~AudioHandler()
{
	if (stream) {
		Pa_AbortStream(stream);
		Pa_CloseStream(stream);
	}
	Pa_Terminate();
}

/******************************************************************************/
/** Utility Member Functions                                                 **/
/******************************************************************************/

/* GetErrorCode
*
* Parameters:
* 		None
*
* Description:
* 		Returns the value of the private member variable
* "errorCode". This can be used to check successful
* conditions to the PA library.
*/
PaError AudioHandler::GetErrorCode(void)
{
	return errorCode;
}

/* PrintErrorMessage
*
* Parameters:
* 		None
*
* Description:
* 		Returns the error string represented by the private
* variable "errorCode"
*/
void AudioHandler::PrintErrorMessage(void)
{
	cout << "  PA Error Code: " << Pa_GetErrorText(errorCode) << endl;
}

/* IsRunning
*
* Parameters:
* 		None
*
* Description:
* 		Returns a boolean where true is status successful
* and false is everything else.
*/
bool AudioHandler::IsRunning(void)
{
	return isRunning;
}

bool AudioHandler::IsNoError(void)
{
	return (errorCode == paNoError);
}


/* IsRunning
*
* Parameters:
* 		bool - Verbosity
*
* Description:
* 		Enables or disables the verbose mode of this object
*/
void AudioHandler::SetVerbosity(bool verb)
{
	verbose = verb;
}


bool AudioHandler::ConfigInput(void)
{
	inputParameters.device = Pa_GetDefaultInputDevice(); /* default input device */
	cout << "Input device #" << inputParameters.device << endl;
	cout << "Input LL :" << Pa_GetDeviceInfo(inputParameters.device)->defaultLowInputLatency << endl;
	cout << "Input HL : " << Pa_GetDeviceInfo(inputParameters.device)->defaultHighInputLatency << endl;

	inputParameters.channelCount = NUM_CHANNELS;
	inputParameters.sampleFormat = PA_SAMPLE_TYPE;
	inputParameters.suggestedLatency = Pa_GetDeviceInfo(inputParameters.device)->defaultHighInputLatency;
	inputParameters.hostApiSpecificStreamInfo = NULL;

	return true;
}

bool AudioHandler::ConnectToInput(void)
{
	/* Open the stream */
	errorCode = Pa_OpenStream(
		&stream,			/* Stream Data Buffer */
		&inputParameters,	/* Input Parameters, NULL for Output Only */
		NULL,				/* Output Parameters, NULL for Input Only */
		SAMPLE_RATE,		/* Sample Rate */
		FRAMES_PER_BUFFER,	/* Frames per Buffer */
		paClipOff,			/* Flags */
		NULL,				/* Input Callback */
		NULL);				/* Output Callback */

	/* Check for error */
	if (errorCode != paNoError) {
		return false;
	}
	
	return true;
}

bool AudioHandler::EnableInput(void)
{
	errorCode = Pa_StartStream(stream);
	if (errorCode != paNoError) {
		return false;
	}

	isRunning = true;
	return true;
}

bool AudioHandler::DisableInput(void)
{
	errorCode = Pa_StopStream(stream);
	isRunning = false;
	if (errorCode != paNoError) {
		return false;
	}

	return true;
}

bool AudioHandler::Run(void)
{
	errorCode = Pa_ReadStream(stream, (void *)buffer.data, FRAMES_PER_BUFFER);
	if (errorCode != paNoError) {
		return false;
	}
	return true;
}

AudioBuffer AudioHandler::GetDataBuffer(void)
{
	return buffer;
}


/******************************************************************************/
/******************************************************************************/
/** Audio Buffer Class                                                       **/
/******************************************************************************/
/******************************************************************************/

AudioBuffer::AudioBuffer(void)
{
	numBytes = FRAMES_PER_BUFFER * NUM_CHANNELS * SAMPLE_SIZE;
	numSamples = FRAMES_PER_BUFFER * NUM_CHANNELS;
	sampleSize = SAMPLE_SIZE;
	sampleFreq = SAMPLE_RATE;
	data = (void *)malloc(numBytes);
}

AudioBuffer::AudioBuffer(const AudioBuffer &rhs)
{
	numBytes = FRAMES_PER_BUFFER * NUM_CHANNELS * SAMPLE_SIZE;
	numSamples = FRAMES_PER_BUFFER * NUM_CHANNELS;
	sampleSize = SAMPLE_SIZE;
	sampleFreq = SAMPLE_RATE;
	data = (void *)malloc(numBytes);

	for (int i = 0; i < numBytes; i++) {
		*((uint8_t *)data + i) = *((uint8_t *)rhs.data + i);
	}
}

AudioBuffer::~AudioBuffer(void)
{
	free(data);
}

/* Get Buffer Pointer
*
* Parameters:
* 		int &
*
* Description:
* 		Returns the void pointer and modifies the paramter with the
* number of bytes the pointer is allocated.
*/
void * AudioBuffer::GetBufferPointer(int & bytes)
{
	bytes = numBytes;
	return data;
}

void AudioBuffer::operator=(const AudioBuffer& rhs)
{
	/* If right hand side is this object, escape */
	if (&rhs == this) {
		return;
	}

	for (int i = 0; i < numBytes; i++) {
		*((uint8_t *)data + i) = *((uint8_t *)rhs.data + i);
	}
}