#pragma once

#include <Windows.h>

#include "LogiKbControl.h"

typedef struct {
	LogiKbControl *kbptr;
	char option;
} dftColorsThreadsParams_t;

unsigned int __stdcall PAThread(void *v);
unsigned int __stdcall DFTcolorThread(void *v);

extern HANDLE mutexHandle;