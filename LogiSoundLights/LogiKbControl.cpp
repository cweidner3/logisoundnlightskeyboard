#include <iostream>
#include <algorithm>	/* For std::min & std::max */

#include <LogitechLEDLib.h>

#include "LogiKbControl.h"

using namespace std;

/******************************************************************************/
/** LogiKbControl Constructors                                               **/
/******************************************************************************/

LogiKbControl::LogiKbControl()
{
	/* Initialize the Led Module */
	if (!LogiLedInit()) {
		cout << "LogiKbControl: Failed to initialize LED SDK" << endl;
	}

	/* Save the LED SDK Version */
	if (!LogiLedGetSdkVersion(&majorNumLed, &minorNumLed, &buildNumLed)) {
		cout << "LogiKbControl: Failed to get SDK Version" << endl;
	}

	/* Save the Current LED settings */
	if (!LogiLedSaveCurrentLighting()) {
		cout << "LogiKbControl: Failed to save LED setting" << endl;
	}

	/* Set the device type to all (the default) */
	if (!LogiLedSetTargetDevice(LOGI_DEVICETYPE_ALL)) {
		cout << "LogiKbControl: Failed to set target device" << endl;
	}
}


LogiKbControl::~LogiKbControl()
{
	/* Restore the initial lighting setting */
	if (!LogiLedRestoreLighting()) {
		cout << "LogiKbControl: Failed to restore LED setting" << endl;
	}

	/* Close the LED module before exiting */
	LogiLedShutdown();
}

/******************************************************************************/
/** LogiKbControl Member Funcitons                                           **/
/******************************************************************************/

void LogiKbControl::run(void)
{
	// TEMP output the SDK versions
	cout << "LED SDK " << majorNumLed << "." << minorNumLed << "." << buildNumLed << endl;
}

bool LogiKbControl::SetKbRGB(RGBcolors colorObj)
{
	if (!LogiLedSetLighting(
		colorObj.GetRp(),
		colorObj.GetGp(),
		colorObj.GetBp())) {
		// Failed to set RGB setting
		return false;
	}
	return true;
}

/******************************************************************************/
/** RGBcolors Constructors                                                   **/
/******************************************************************************/

RGBcolors::RGBcolors()
	: red(0) , green(0) , blue(0)
{
	redp = 0; greenp = 0; bluep = 0;
}

RGBcolors::RGBcolors(int _red, int _green, int _blue)
	: red(_red), green(_green), blue(_blue)
{
	redp = (red * 100) / 255;
	greenp = (green * 100) / 255;
	bluep = (blue * 100) / 255;
}

RGBcolors::~RGBcolors()
{
}

/******************************************************************************/
/** LogiKbControl Member Functions                                           **/
/******************************************************************************/

/* HSL to RGB
 * Converts the Hue, Saturation, Lightness values to Red, Green, Blue values
 *
 * Value ranges
 *	R [0..255]
 *  G [0..255]
 *  B [0..255]
 *  H [0..359]
 *  S [0..255] default 100%
 *  L [0..255] default 50%
 */
void RGBcolors::HSLtoRGB(int H, int S, int L)
{
	double k;

	/* Make sure  HSL are between 0 and 240 */
	H = min(240, H); S = min(240, S); L = min(240, L);
	H = max(0, H); S = max(0, S); L = max(0, L);

	/* Set the Red Component */
	if (H < 80) {
		SetR(min(255, 255 * (80 - H) / 40));
	}
	else if (H > 160) {
		SetR(min(255, 255 * (H - 160) / 40));
	}

	/*  Set the Green Component */
	if (H < 160) {
		SetG(min(255, 255 * (80 - abs(H - 80)) / 40));
	}

	/* Set the Blue Component */
	if (H > 80) {
		SetB(min(255, 255 * (80 - abs(H - 160)) / 40));
	}

	/* Modify the RGB values according to Saturation */
	if (S < 240) {
		k = (double)S / 240.0;
		SetRGB(
			(int)((double)GetR() * k),
			(int)((double)GetG() * k),
			(int)((double)GetB() * k));
		k = (128.0 * (240.0 - (double)S)) / 240.0;
		SetRGB(
			(int)((double)GetR() + k),
			(int)((double)GetG() + k),
			(int)((double)GetB() + k));
	}

	/* Set the RGB values based on the Lightness */
	k = (120.0 - abs(((double)L - 120.0))) / 120.0;
	SetRGB(
		(int)((double)GetR() * k),
		(int)((double)GetG() * k),
		(int)((double)GetB() * k));
	if (L > 120) {
		k = 256.0 * ((double)L - 120.0) / 120.0;
		SetRGB(
			(int)((double)GetR() + k),
			(int)((double)GetG() + k),
			(int)((double)GetB() + k));
	}
}

/******************************************************************************/

void RGBcolors::HSVtoRGB(int _H, int _S, int _V)
{
	int i;
	float f, p, q, t, h, s, v, r, g, b;

	h = (float)_H;
	s = (float)_S;
	v = (float)_V;

	s /= 255;

	if (s == 0) { // achromatic (grey)
		SetR((int)v);
		SetG((int)v);
		SetB((int)v);
		return;
	}

	h /= 60;            // sector 0 to 5
	i = floor(h);
	f = h - i;            // factorial part of h
	p = (uint8_t)(v * (1 - s));
	q = (uint8_t)(v * (1 - s * f));
	t = (uint8_t)(v * (1 - s * (1 - f)));

	switch (i) {
	case 0:
		r = v;
		g = t;
		b = p;
		break;
	case 1:
		r = q;
		g = v;
		b = p;
		break;
	case 2:
		r = p;
		g = v;
		b = t;
		break;
	case 3:
		r = p;
		g = q;
		b = v;
		break;
	case 4:
		r = t;
		g = p;
		b = v;
		break;
	default:        // case 5:
		r = v;
		g = p;
		b = q;
		break;
	}

	SetR((int)r);
	SetG((int)g);
	SetB((int)b);
}

/******************************************************************************/

/* SetRGB
 * Sets the red green blue spectrum in one call.
 */
void RGBcolors::SetRGB(int R, int G, int B)
{
	red = R; green = G; blue = B;
	redp	= (int)(((double)red   * 100.0) / 255.0);
	greenp	= (int)(((double)green * 100.0) / 255.0);
	bluep	= (int)(((double)blue  * 100.0) / 255.0);
}

/* SetR SetG SetB
 * Sets the red green blue spectrum individually.
 */
void RGBcolors::SetR(int R)
{
	red = R;
	redp = (int)(((double)red   * 100.0) / 255.0);
}
void RGBcolors::SetG(int G)
{
	green = G;
	greenp = (int)(((double)green * 100.0) / 255.0);
}
void RGBcolors::SetB(int B)
{
	blue = B;
	bluep = (int)(((double)blue  * 100.0) / 255.0);
}

/* GetR GetG GetB
* Gets the red green blue spectrum hex values individually.
*/
int RGBcolors::GetR(void)
{
	return red;
}
int RGBcolors::GetG(void)
{
	return green;
}
int RGBcolors::GetB(void)
{
	return blue;
}

/* GetR GetG GetB
* Gets the red green blue spectrum percent values individually.
*/
int RGBcolors::GetRp(void)
{
	return redp;
}
int RGBcolors::GetGp(void)
{
	return greenp;
}
int RGBcolors::GetBp(void)
{
	return bluep;
}

/******************************************************************************/
/** NOTES                                                                    **/
/******************************************************************************/

#if 0
--HSL to RGB color space conversion
-- you don't need to understand this
function HSLtoRGB(H, S, L)
	H, S, L = math.min(240, H), math.min(240, S), math.min(240, L)
	H, S, L = math.max(0, H), math.max(0, S), math.max(0, L)
	local R, G, B = 0, 0, 0
	if H < 80 then
		R = math.min(255, 255 * (80 - H) / 40)
	elseif H > 160 then
		R = math.min(255, 255 * (H - 160) / 40)
	end
	if H < 160 then
		G = math.min(255, 255 * (80 - math.abs(H - 80)) / 40)
	end
	if H > 80 then
		B = math.min(255, 255 * (80 - math.abs(H - 160)) / 40)
	end
	if S < 240 then
		k = S / 240
		R, G, B = R*k, G*k, B*k
		k = 128 * (240 - S) / 240
		R, G, B = R + k, G + k, B + k
	end
	k = (120 - math.abs(L - 120)) / 120
	R, G, B = R*k, G*k, B*k
	if L > 120 then
		k = 256 * (L - 120) / 120
		R, G, B = R + k, G + k, B + k
	end
	return R, G, B
end

#endif


#if 0
/******************************************************************************/
/* RGB HSV Functions                                                          */
/******************************************************************************/

// <editor-fold defaultstate="collapsed" desc="RGB to HSV">
/*******************************************************************************
* Function RGB2HSV
* Description: Converts an RGB color value into its equivalen in the HSV color space.
* Copyright 2010 by George Ruinelli
* The code I used as a source is from http://www.cs.rit.edu/~ncs/color/t_convert.html
* Parameters:
*   1. struct with RGB color (source)
*   2. pointer to struct HSV color (target)
* Notes:
*   - r, g, b values are from 0..255
*   - h = [0,360], s = [0,255], v = [0,255]
*   - NB: if s == 0, then h = 0 (undefined)
******************************************************************************/
void RGB2HSV(RGB_t RGB, HSV_t *HSV) {
		uint8_t min, max, delta;

		if (RGB.r<RGB.g)
			min = RGB.r;
		else
			min = RGB.g;
		if (RGB.b<min)
			min = RGB.b;

		if (RGB.r>RGB.g)
			max = RGB.r;
		else
			max = RGB.g;
		if (RGB.b>max)
			max = RGB.b;

		HSV->v = max;                // v, 0..255

		delta = max - min;                      // 0..255, < v

		if (max != 0)
			HSV->s = (int16_t)(delta)* 255 / max;        // s, 0..255
		else {
			// r = g = b = 0        // s = 0, v is undefined
			HSV->s = 0;
			HSV->h = 0;
			return;
		}

		if (RGB.r == max)
			HSV->h = (RGB.g - RGB.b) * 60 / delta;        // between yellow & magenta
		else if (RGB.g == max)
			HSV->h = 120 + (RGB.b - RGB.r) * 60 / delta;    // between cyan & yellow
		else
			HSV->h = 240 + (RGB.r - RGB.g) * 60 / delta;    // between magenta & cyan

		if (HSV->h < 0)
			HSV->h += 360;
	}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="HSV to RGB">
/*******************************************************************************
* Function HSV2RGB
* Description: Converts an HSV color value into its equivalen in the RGB color space.
* Copyright 2010 by George Ruinelli
* The code I used as a source is from http://www.cs.rit.edu/~ncs/color/t_convert.html
* Parameters:
*   1. struct with HSV color (source)
*   2. pointer to struct RGB color (target)
* Notes:
*   - r, g, b values are from 0..255
*   - h = [0,360], s = [0,255], v = [0,255]
*   - NB: if s == 0, then h = 0 (undefined)
******************************************************************************/
void HSV2RGB(HSV_t HSV, RGB_t *RGB) {
	int i;
	float f, p, q, t, h, s, v;

	h = (float)HSV.h;
	s = (float)HSV.s;
	v = (float)HSV.v;

	s /= 255;

	if (s == 0) { // achromatic (grey)
		RGB->r = RGB->g = RGB->b = v;
		return;
	}

	h /= 60;            // sector 0 to 5
	i = floor(h);
	f = h - i;            // factorial part of h
	p = (uint8_t)(v * (1 - s));
	q = (uint8_t)(v * (1 - s * f));
	t = (uint8_t)(v * (1 - s * (1 - f)));

	switch (i) {
	case 0:
		RGB->r = v;
		RGB->g = t;
		RGB->b = p;
		break;
	case 1:
		RGB->r = q;
		RGB->g = v;
		RGB->b = p;
		break;
	case 2:
		RGB->r = p;
		RGB->g = v;
		RGB->b = t;
		break;
	case 3:
		RGB->r = p;
		RGB->g = q;
		RGB->b = v;
		break;
	case 4:
		RGB->r = t;
		RGB->g = p;
		RGB->b = v;
		break;
	default:        // case 5:
		RGB->r = v;
		RGB->g = p;
		RGB->b = q;
		break;
	}
}
#endif