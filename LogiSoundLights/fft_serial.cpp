//****************************************************************************80
//
// The Contents of this document was obtained from the following website
// https://people.sc.fsu.edu/~jburkardt/cpp_src/fft_serial/fft_serial.html
//
// The code was distributed under the GNU LGPL license
// 
//****************************************************************************80
#include "fft_serial.h"

// Avoid the deprecation
// localtime_s is apparently an alternative to localtime
#define _CRT_SECURE_NO_WARNINGS

# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <cmath>
# include <ctime>
# include <omp.h>

using namespace std;


//****************************************************************************80

void ccopy(int n, double x[], double y[])

//****************************************************************************80
//
//  Purpose:
//
//    CCOPY copies a complex vector.
//
//  Discussion:
//
//    The "complex" vector A[N] is actually stored as a double vector B[2*N].
//
//    The "complex" vector entry A[I] is stored as:
//
//      B[I*2+0], the real part,
//      B[I*2+1], the imaginary part.
//
//  Modified:
//
//    23 March 2009
//
//  Author:
//
//    Original C version by Wesley Petersen.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Wesley Petersen, Peter Arbenz, 
//    Introduction to Parallel Computing - A practical guide with examples in C,
//    Oxford University Press,
//    ISBN: 0-19-851576-6,
//    LC: QA76.58.P47.
//
//  Parameters:
//
//    Input, int N, the length of the "complex" array.
//
//    Input, double X[2*N], the array to be copied.
//
//    Output, double Y[2*N], a copy of X.
//
{
	int i;

	for (i = 0; i < n; i++)
	{
		y[i * 2 + 0] = x[i * 2 + 0];
		y[i * 2 + 1] = x[i * 2 + 1];
	}
	return;
}
//****************************************************************************80

void cfft2(int n, double x[], double y[], double w[], double sgn)

//****************************************************************************80
//
//  Purpose:
//
//    CFFT2 performs a complex Fast Fourier Transform.
//
//  Modified:
//
//    23 March 2009
//
//  Author:
//
//    Original C version by Wesley Petersen.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Wesley Petersen, Peter Arbenz, 
//    Introduction to Parallel Computing - A practical guide with examples in C,
//    Oxford University Press,
//    ISBN: 0-19-851576-6,
//    LC: QA76.58.P47.
//
//  Parameters:
//
//    Input, int N, the size of the array to be transformed.
//
//    Input/output, double X[2*N], the data to be transformed.  
//    On output, the contents of X have been overwritten by work information.
//
//    Output, double Y[2*N], the forward or backward FFT of X.
//
//    Input, double W[N], a table of sines and cosines.
//
//    Input, double SGN, is +1 for a "forward" FFT and -1 for a "backward" FFT.
//
{
	int j;
	int m;
	int mj;
	int tgle;

	m = (int)(log((double)n) / log(1.99));
	mj = 1;
	//
	//  Toggling switch for work array.
	//
	tgle = 1;
	step(n, mj, &x[0 * 2 + 0], &x[(n / 2) * 2 + 0], &y[0 * 2 + 0], &y[mj * 2 + 0], w, sgn);

	if (n == 2)
	{
		return;
	}

	for (j = 0; j < m - 2; j++)
	{
		mj = mj * 2;
		if (tgle)
		{
			step(n, mj, &y[0 * 2 + 0], &y[(n / 2) * 2 + 0], &x[0 * 2 + 0], &x[mj * 2 + 0], w, sgn);
			tgle = 0;
		}
		else
		{
			step(n, mj, &x[0 * 2 + 0], &x[(n / 2) * 2 + 0], &y[0 * 2 + 0], &y[mj * 2 + 0], w, sgn);
			tgle = 1;
		}
	}
	//
	//  Last pass thru data: move y to x if needed 
	//
	if (tgle)
	{
		ccopy(n, y, x);
	}

	mj = n / 2;
	step(n, mj, &x[0 * 2 + 0], &x[(n / 2) * 2 + 0], &y[0 * 2 + 0], &y[mj * 2 + 0], w, sgn);

	return;
}
//****************************************************************************80

void cffti(int n, double w[])

//****************************************************************************80
//
//  Purpose:
//
//    CFFTI sets up sine and cosine tables needed for the FFT calculation.
//
//  Modified:
//
//    23 March 2009
//
//  Author:
//
//    Original C version by Wesley Petersen.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Wesley Petersen, Peter Arbenz, 
//    Introduction to Parallel Computing - A practical guide with examples in C,
//    Oxford University Press,
//    ISBN: 0-19-851576-6,
//    LC: QA76.58.P47.
//
//  Parameters:
//
//    Input, int N, the size of the array to be transformed.
//
//    Output, double W[N], a table of sines and cosines.
//
{
	double arg;
	double aw;
	int i;
	int n2;
	const double pi = 3.141592653589793;

	n2 = n / 2;
	aw = 2.0 * pi / ((double)n);

	for (i = 0; i < n2; i++)
	{
		arg = aw * ((double)i);
		w[i * 2 + 0] = cos(arg);
		w[i * 2 + 1] = sin(arg);
	}
	return;
}
//****************************************************************************80

double cpu_time(void)

//****************************************************************************80
//
//  Purpose:
// 
//    CPU_TIME reports the elapsed CPU time.
//
//  Modified:
//
//    27 September 2005
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Output, double CPU_TIME, the current total elapsed CPU time in second.
//
{
	double value;

	value = (double)clock() / (double)CLOCKS_PER_SEC;

	return value;
}
//****************************************************************************80

double ggl(double *seed)

//****************************************************************************80
//
//  Purpose:
//
//    GGL generates uniformly distributed pseudorandom numbers. 
//
//  Modified:
//
//    23 March 2009
//
//  Author:
//
//    Original C version by Wesley Petersen, M Troyer, I Vattulainen.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Wesley Petersen, Peter Arbenz, 
//    Introduction to Parallel Computing - A practical guide with examples in C,
//    Oxford University Press,
//    ISBN: 0-19-851576-6,
//    LC: QA76.58.P47.
//
//  Parameters:
//
//    Input/output, double *SEED, used as a seed for the sequence.
//
//    Output, double GGL, the next pseudorandom value.
//
{
	double d2 = 0.2147483647e10;
	double t;
	double value;

	t = *seed;
	t = fmod(16807.0 * t, d2);
	*seed = t;
	value = (t - 1.0) / (d2 - 1.0);

	return value;
}
//****************************************************************************80

void step(int n, int mj, double a[], double b[], double c[],
	double d[], double w[], double sgn)

	//****************************************************************************80
	//
	//  Purpose:
	//
	//    STEP carries out one step of the workspace version of CFFT2.
	//
	//  Modified:
	//
	//    23 March 2009
	//
	//  Author:
	//
	//    Original C version by Wesley Petersen.
	//    C++ version by John Burkardt.
	//
	//  Reference:
	//
	//    Wesley Petersen, Peter Arbenz, 
	//    Introduction to Parallel Computing - A practical guide with examples in C,
	//    Oxford University Press,
	//    ISBN: 0-19-851576-6,
	//    LC: QA76.58.P47.
	//
	//  Parameters:
	//
{
	double ambr;
	double ambu;
	int j;
	int ja;
	int jb;
	int jc;
	int jd;
	int jw;
	int k;
	int lj;
	int mj2;
	double wjw[2];

	mj2 = 2 * mj;
	lj = n / mj2;

	for (j = 0; j < lj; j++)
	{
		jw = j * mj;
		ja = jw;
		jb = ja;
		jc = j * mj2;
		jd = jc;

		wjw[0] = w[jw * 2 + 0];
		wjw[1] = w[jw * 2 + 1];

		if (sgn < 0.0)
		{
			wjw[1] = -wjw[1];
		}

		for (k = 0; k < mj; k++)
		{
			c[(jc + k) * 2 + 0] = a[(ja + k) * 2 + 0] + b[(jb + k) * 2 + 0];
			c[(jc + k) * 2 + 1] = a[(ja + k) * 2 + 1] + b[(jb + k) * 2 + 1];

			ambr = a[(ja + k) * 2 + 0] - b[(jb + k) * 2 + 0];
			ambu = a[(ja + k) * 2 + 1] - b[(jb + k) * 2 + 1];

			d[(jd + k) * 2 + 0] = wjw[0] * ambr - wjw[1] * ambu;
			d[(jd + k) * 2 + 1] = wjw[1] * ambr + wjw[0] * ambu;
		}
	}
	return;
}
//****************************************************************************80

void timestamp()

//****************************************************************************80
//
//  Purpose:
//
//    TIMESTAMP prints the current YMDHMS date as a time stamp.
//
//  Example:
//
//    31 May 2001 09:45:54 AM
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license. 
//
//  Modified:
//
//    24 September 2003
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    None
//
{
# define TIME_SIZE 40

	static char time_buffer[TIME_SIZE];
	const struct tm *tm;
	size_t len;
	time_t now;

	now = time(NULL);
	tm = localtime(&now);

	len = strftime(time_buffer, TIME_SIZE, "%d %B %Y %I:%M:%S %p", tm);

	cout << time_buffer << "\n";

	return;
# undef TIME_SIZE
}