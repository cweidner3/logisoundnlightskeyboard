#pragma once

#include <portaudio.h>
#include <stdio.h>

class AudioBuffer
{
public:
	AudioBuffer();						// Default Constructor
	AudioBuffer(const AudioBuffer &);	// Copy Constructor
	~AudioBuffer();						// Destructor

	void * GetBufferPointer(int &);
	void operator= (const AudioBuffer  &);

public:
	int numBytes;
	int numSamples;
	int sampleSize;
	int sampleFreq;
	void *data;
};

class AudioHandler
{
public:
	AudioHandler();
	AudioHandler(bool);
	~AudioHandler();

	PaError GetErrorCode(void);
	void PrintErrorMessage(void);
	bool IsRunning(void);
	bool IsNoError(void);
	void SetVerbosity(bool);

	bool ConnectToInput(void);
	bool ConfigInput(void);
	bool EnableInput(void);
	bool DisableInput(void);
	bool Run(void);

	AudioBuffer GetDataBuffer(void);

private: 
	bool isRunning;
	bool verbose;
	/* PA Members */
	PaStreamParameters inputParameters;
	PaStream *stream = NULL;
	PaError errorCode;
	AudioBuffer buffer;
	int numBytes;

};
