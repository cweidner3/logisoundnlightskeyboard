#pragma once

/* CCOPY copies a complex vector. */
void ccopy(int n, double x[], double y[]);

/* CFFT2 performs a complex Fast Fourier Transform. */
void cfft2(int n, double x[], double y[], double w[], double sgn);

/* CFFTI sets up sine and cosine tables needed for the FFT calculation. */
void cffti(int n, double w[]);

/* CPU_TIME reports the elapsed CPU time. */
double cpu_time(void);

/* GGL generates uniformly distributed pseudorandom numbers. */
double ggl(double *ds);

/* STEP carries out one step of the workspace version of CFFT2. */
void step(int n, int mj, double a[], double b[], double c[], double d[],
	double w[], double sgn);

/* TIMESTAMP prints the current YMDHMS date as a time stamp. */
void timestamp();