/******************************************************************************/
/** Includes                                                                 **/
/******************************************************************************/

/* System Includes */
#include <Windows.h>
#include <process.h>
#include <iostream>
#include <tlhelp32.h>	/* For Checking if process is running */
//#include <atlstr.h>		/* For the CString Type */
#include <numeric>		/* FOr std::accumulate */
#include <conio.h>		/* For _getch() */


/* Custom Includes */
#include "LogiKbControl.h"
#include "AudioHandler.h"
#include "DftColorsThreads.h"

#include <portaudio.h>

/******************************************************************************/
/** Global Definitions                                                       **/
/******************************************************************************/

#define AFX_IDS_APP_TITLE "LogiSoundLights.exe"

/******************************************************************************/
/** Local Prototypes                                                         **/
/******************************************************************************/

unsigned int __stdcall CycleLedThread(void*);
unsigned int __stdcall CLIThread(void *v);

BOOL AppIsAllreadyRunning(BOOL bShow/*=TRUE*/);

HANDLE cycleLedHandle, cliHandle;
bool mainRunning = true;

/* Threads Exit Condition */
bool globalKeepRunning = false;

/******************************************************************************/
/** Main Routine                                                             **/
/******************************************************************************/

using namespace std;

int main(int argc, char *argv[]) {
	if (AppIsAllreadyRunning(false)) {
		cout << "App is Already Running" << endl;
		return 0;
	}

	if (argc > 1) {
		/* We have arguments to deal with */
	}

	//AudioHandler audioObj;

	cliHandle = (HANDLE)_beginthreadex(0, 0, &CLIThread, 0, 0, 0);

	/* Chill here till the program is done */
	while (mainRunning)
		;

	CloseHandle(cliHandle);

	return 0;
}


/******************************************************************************/
/** Cycle LED Thread                                                         **/
/******************************************************************************/

unsigned int __stdcall CycleLedThread(void *v)
{
	LogiKbControl *kbObject = (LogiKbControl *)v;
	int i = 0, n = 0;
	int H = 0, S = 240, L = 120;
	RGBcolors rgbObj;
	
	/* Provide Light Cycling Effect */
	while (kbObject->keepRunning) {
		// Reset the Hue loop if needed
		if (H++ > 240) {
			H = 0;
		}

		// Determine the new RGB values
		rgbObj.HSLtoRGB(H, S, L);

		// Set the RGB values to the logitech keyboard
		if (!kbObject->SetKbRGB(rgbObj)) {
			cout << "Failed to send RGB values to KB" << endl;
		}

		// Wait a moment before looping
		Sleep(50);
	}
	
	return 0;
}


/******************************************************************************/
/** Command Line Interface Thread                                            **/
/******************************************************************************/

unsigned int __stdcall CLIThread(void *v)
{
	char usrChar = 0;
	bool keepRunning = false;
	LogiKbControl ledCtrlObj;
	RGBcolors colorObj(255, 255, 255);

	string menuString = "==Menu==\n"
		"r,g,b - Set Color   c - Cycle\n"
		"d - DFT Colors      h - HSV Colors\n"
		"q - Quit\n";

	do {
		cout << menuString.c_str();
		cout << "Choose Option: ";
		usrChar = _getch();
		//getchar();

		switch (usrChar) {
		case 'r':
			colorObj.SetRGB(255, 0, 0);
			ledCtrlObj.SetKbRGB(colorObj);
			break;
		case 'g':
			colorObj.SetRGB(0, 255, 0);
			ledCtrlObj.SetKbRGB(colorObj);
			break;
		case 'b':
			colorObj.SetRGB(0, 0, 255);
			ledCtrlObj.SetKbRGB(colorObj);
			break;
		case 'c': {
			/* Set keepRunning flag */
			ledCtrlObj.keepRunning = true;

			/* Create the processing thread */
			cycleLedHandle = (HANDLE)_beginthreadex(0, 0, &CycleLedThread, (void *)&ledCtrlObj, 0, 0);

			/* Wait for user input before exiting */
			cout << "Press 'q' then ENTER to exit" << endl;
			while (ledCtrlObj.keepRunning) {
				usrChar = _getch();
				//getchar();
				if (usrChar == 'q') {
					usrChar = 'c';
					ledCtrlObj.keepRunning = false;
				}
			}

			/* Close the thread */
			CloseHandle(cycleLedHandle);

			/* Set the previous color */
			ledCtrlObj.SetKbRGB(colorObj);
			break;
		}
		case 'd':
		case 'h': {
			//cout << "Not Implemented" << endl;
			/* Setup Objects */
			HANDLE audioHandle, colorHandle;
			AudioHandler audioObj;
			keepRunning = true;
			globalKeepRunning = true;
			dftColorsThreadsParams_t threadargs = {&ledCtrlObj, usrChar};

			/* Create Mutex (Not Named) */
			mutexHandle = CreateMutex(NULL, FALSE, NULL);

			/* Call Threads */
			audioHandle = (HANDLE)_beginthreadex(0, 0, &PAThread, (void *)&audioObj, 0, 0);
			colorHandle = (HANDLE)_beginthreadex(0, 0, &DFTcolorThread, (void *)&threadargs, 0, 0);
			
			/* Wait for user input before exiting */
			cout << "Press 'q' then ENTER to exit" << endl;
			while (keepRunning) {
				usrChar = _getch();
				//getchar();
				if (usrChar == 'q') {
					usrChar = 'd';
					keepRunning = false;
					globalKeepRunning = false;
				}
			}

			/* Wait for Threads to close out */
			WaitForSingleObject(audioHandle, INFINITE);
			WaitForSingleObject(colorHandle, INFINITE);

			/* Close  the threads */
			CloseHandle(audioHandle);
			CloseHandle(colorHandle);

			/* Set the previous color */
			ledCtrlObj.SetKbRGB(colorObj);
			break;
		}
		}

	} while (usrChar != 'q');

	mainRunning = false;

	return 0;
}



/******************************************************************************/
/** Main Process Function Definitions                                        **/
/******************************************************************************/

#if 0
// Helper callback-function for function AppIsAllreadyRunning()
// see description of EnumWindows() for details
BOOL CALLBACK ShowAppEnum(HWND hwnd, LPARAM lParam)
{
	DWORD dwID;
	CString strAppName;
	strAppName.LoadString(AFX_IDS_APP_TITLE);
	GetWindowThreadProcessId(hwnd, &dwID);
	if (dwID == (DWORD)lParam)
	{
		char title[256];
		CString strBuffer;
		GetWindowText(hwnd, title, 256);
		strBuffer = title;
		if (strBuffer.Left(strAppName.GetLength()) == strAppName)
		{
			if (!IsWindowVisible(hwnd))
				ShowWindow(hwnd, SW_SHOW);
			SetForegroundWindow(hwnd);
		}
	}
	return TRUE;
}
#endif

/*
 * Checks, if an application with this name is running
 *
 * bShow ..... TRUE: bring application to foreground, if running 
 *             FALSE: only check, don't move to the application
 *
 * return: FALSE: application is not running
 *         TRUE: application runs
 */
BOOL AppIsAllreadyRunning(BOOL bShow/*=TRUE*/)
{
	BOOL bRunning = FALSE;
	char strAppName[] = AFX_IDS_APP_TITLE;
//	CString strAppName;
//	strAppName.LoadString(AFX_IDS_APP_TITLE);
//	strAppName += _T(".exe");
	DWORD dwOwnPID = GetProcessId(GetCurrentProcess());
	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32* processInfo = new PROCESSENTRY32;
	processInfo->dwSize = sizeof(PROCESSENTRY32);
	int index = 0;
	while (Process32Next(hSnapShot, processInfo) != FALSE)
	{
		if (!strcmp(processInfo->szExeFile, strAppName))
		{
			if (processInfo->th32ProcessID != dwOwnPID)
			{
#if 0
				if (bShow)
					EnumWindows(ShowAppEnum, processInfo->th32ProcessID);
#endif
				bRunning = TRUE;
				break;
			}
		}
	}
	CloseHandle(hSnapShot);
	delete processInfo;
	return bRunning;
}