## Logitech Sound n Light Keyboard

Uses MSVC C++ along with the PortAudio and Logitech GSDK to gather the audio going to the sound card and use the current frequencies in the audio signal to control the LEDs on the Logitech gaming keyboard.

## Special Note

The final binary file requires the portaudio_x86.dll to be present in the same directory as the binary for the execution of the program. The program also requires the installation of the Logitech Gaming Software (_LGS_) before execution.